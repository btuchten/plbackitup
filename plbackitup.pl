#!/usr/bin/perl
#
# plbackitup.pl v0.7.1
# Written by Brian Tuchten
# Last modified: 01/09/2020
###################################################
# Permission to use, copy, and distribute this software and its
# documentation for any purpose with or without fee is hereby granted,
# provided that the above copyright notice appear in all copies and
# that both that copyright notice and this permission notice appear
# in supporting documentation.
#
# Permission to modify the software is granted, but not the right to
# distribute the modified code.  Modifications are to be distributed
# as patches to released version.
#
# This software is provided "as is" without
# express or implied warranty.
#
# Use at your own risk. 
#####################################################
# Info:
# Tars up directories to a given backup folder
# I wrote it to run as a cron for backup jobs using
# 0 1 * * *       /usr/local/backup/plbackitup.pl (uniquename) > /dev/null 2>&1
# Tailor it to how you want and test it before putting it in the cron
#
# For more info on using an ARGV (unique name)
# look at the comments for the VAR $remove_old_files
#
#
# Check all the paths in the VARS section
# to your systems specifications. Also change the path
# to the  $backup_dir and $backup_log
#
# See the file README.changelog for changes
#
# Tested on Slackware v7.1
#
# **Note: This script uses the date command "--date n days ago"
#         to determine when to delete old files.  This command may
#	  not work on all Unix type systems.
#
###############################
###############################
#
#To use the scp function, you need to have ssh installed on the machine running
#the the plbackitup script, and ssh installed on the remote server your copying the
#files over to.  You can download ssh from www.ssh.com
#Tested with ssh-1.2.30.tar.gz
#
# ALSO you MUST have a trust already established between the 2 machines, which
# means no password is required to send the files securely.
# To do this :
#	1. Make sure the full host name and IP (for the machine running the
#	   script) is in your /etc/hosts file on the remote machine
#	2. ssh from host1 to remotehost as the account name which will own
#	   the secure copied files (to get the key in place)
#	3. Now create a file ~home/.shosts on the remote machine (which you're 
#	   now on) and put in there 'fullhostname username' (without the quotes)
#	   The fullhostname is the host which will run the script. Chmod it to 600, 
#	   and make sure it is owned by the account.
#	4. Copy the key in your ~home/.ssh/known_hosts for the same host (as above)
#	   to ~home/.ssh/authorized_hosts. Chmod it to 600, and make sure it is
#	   owned by the account.
#	5. Edit ~home/.ssh/authorized_hosts (copy) and delete the hostname (of the host
#	   running the script) from the beginning of the line (including the space 
#	   after the hostname)
#	6. At the END of the line put ' username@hostname' (without the quotes and yes
#          there is a space there) the hostname being the one you copied from step 5.
#	7. TEST IT!  You should be able to ssh or scp from the host running the
#	   script to the remotehost without entering a password now.
#	8. IF you are running this script as a cron for another user (ex: root)
#	   then you also need to add the name (ex: root) to the file in
#	   step 3 on another line.  I don't recommend running the script as root 
#	   and also scp'ing the files as root. Use another account on the
#	   remote machine.
#
#	If you can't get this to work, don't use scp. The script will sit there waiting
#	for a password.
#
##############################
##############################
##
##   USAGE:
##  
##   /path/plbackitup.pl  /path_to_backup_list_name/backup_listname  [unique_file_name]
##
##   [unique_file_name] is optional
##
##   >>>>>> It is suggested you run the script manually prior to putting it in a cron job.
##
##


open (STDERR,'>&STDOUT');
$| = 1;


# to use the Perl FTP module download and install the libnet module from www.perl.com
# under CPAN - Modules
# tested with http://www.perl.com/CPAN-local/authors/id/G/GB/GBARR/libnet-1.0703.tar.gz
# otherwise comment out the line below. May have to comment out others too.
use Net::FTP;

$script = $ENV{'SCRIPT_NAME'};			# script name (don't think this works)
$ENV{"PATH"} = "$PATH:/bin:/sbin:/usr/bin";	# set the env path so cron
						#  runs correctly


################
# Command Vars
################
$scp_path = "/usr/local/bin/scp";		# path to "scp" 
$ftp_path = "/bin/ftp";				# path to "ftp"
$date_path = "/bin/date";			# path to "date"
$touch = "/bin/touch";				# path to "touch"
$tar = "/bin/tar";				# path to "tar"
$delete = "/bin/rm";				# path to "rm"
$dir = "/bin/ls";				# path to "ls"
$email_path = "/bin/mailx";			# path to "xmail"


################
## Backup Vars
################
$backup_dir = "/tmp/backup/";                   # path to dir to save your
						#  backed up files to (end with /)

################
## Misc Vars
################
$date_format = "+%d%b%Y";                       # dateformat ddMonYear for file name
$tar_options = "-czvf";                         # options for tar (see man page)
$tar_excl_cmd = "--exclude";                    # The option for tar to exclude
$file_end = ".tgz";                             # file extension, change to .tar.gz if you want

$fd = "-";                                      # Backup file name (delimiter) goes between 
						#  the date stamp and file name ($partialname),
						#  and the unique name if choosing that option


################
## Log Vars
################
$backup_log = "/tmp/backup/.plbackitup.log";    # path to the log file

$email_log = 0;					# Email the log?	
						#  0 = no don't email log	
						#  1 = yes email log
						#  I don't recommend emailing the log
						#  in verbose mode (sometimes HUGE!)

$email_to = "root\@localhost";                  # email address to send the
						# log to (don't forget the \@)

$type_log = 1;                                  # type of log   
                                                #  0 = no log
                                                #  1 = date/time file info
                                                #  2 = verbose (all output)

$overwrite_log = 0;                             # overwrite the log file        
                                                #  0 = no (append)
                                                #  1 = yes
                                                # might want to overwrite if
                                                # emailing the log

################
## Secure Copy
## Vars
################
#
# See the detailed instructions
# above for getting this to work.
#

$use_scp = 0;					# 0 = don't scp files to remote server
						# 1 = scp files to remote server
						# NOTE: This only copies the current files,
						# NOT all the files in the DIR $backup_dir
						# See instructions above for help in getting
						# this setup

$scp_all_files = 0;				# only works if $use_scp = 1
						#  0 = scp only the new backed up files
						#  1 = scp ALL the files in the DIR $backup_dir
						#  matching $file_end

$scp_hostname = "localhost";			# name of the remote server to scp to
$scp_username = "username";			# change this to the remote account for scp
						# no password is needed because you should
						#  already have a trust between the 2 systems

$scp_remote_dir = "/tmp/backup/";		# change this to the remote DIR you want
						#  to scp the files to on the server $scp_hostname
						#  the account $scp_username must have read/write
						#  to the DIR $scp_remote_dir.  This is the
						#  absolute path on the remote server.

################
## FTP Vars
################
$use_ftp = 0;					# 0 = don't ftp files
						# 1 = ftp files somewhere
						# NOTE: This only FTPs the current files,
						# NOT all the files in the DIR $backup_dir

$ftp_all_files = 0;				# only works if $use_ftp = 1	
						#  0 = ftp only the new backedup files
						#  1 = ftp ALL the files in the DIR $backup_dir

$ftp_hostname = 'localhost';			# name of the ftp server to upload the
						#  backup files to

$ftp_username = 'anonymous';			# ftp account name (you prob don't wanna use
						#  anonymous

$ftp_password = 'someone@foo.com';		# change to the account's passwd (above)
$ftp_remote_dir = '/incoming';			# Remote directory to put files
						#  This is relative to the CHROOT
						#  for the account listed in $ftp_username


$ftp_debug = 0;					# Console (only) debug output for ftp MODULE
						#  0 = no debugging (with non module output)
						#  1 = some debugging output (no non module output)
						#  2 = verbose output (probably SAA)
						#  (thats what a friend told me but 1 and 2 look
						#  the same, so if you want to see the output, 
						#  I suggest setting it to 1)
						#  There is already some output to the
						#  screen and log now that I added in.



$ftp_timeout = 600;				# This is the number of seconds
						#  of the ftp connections ABSOLUTE timeout,
						#  not just the connection wait.  If you have a low
						#  bandwidth connection, or are transferring a
						#  high amount of data, you may want to set this higher
						#  to something like 3000 or 3600 seconds.


################
## Deletion Vars
################
$remove_old_files = 0;				# 0 = delete no local backup files 	
						# 1 = delete ALL local backup files
						#  in matching $file_end in $backup_dir
						# 2 = delete local backup files in 
						#  the DIR $backup_dir with the
						#  extension $file_end older than
						#  "$date_check" days ago (including files
						#  from TODAY'S DATE)
						# 3 = keep $date_check number of backup files of each
						#  when ising a unique name passed by
						#  ARG[1]. The ARG[1] Value being a different
						#  name each time the script is run.
						#  Use this as an option  if you run a backup 
						#  a few times a day.  To keep multiple files 
						#  from the same day, Run the job as: (example)
						#  1st time "/dir/plbackitup.pl /dir/plbackup_list backup1"
						#  2nd as "/dir/plbackitup.pl /dir/plbackup_list backup2" 
						#  and the script will put the ARG[1] into the filename
						#  to make it unique. See FAQ.plbackitup for more
						#  info

$date_check = 10;				# number of days to keep old backup files.
						#  This is now determined by the date stamp
						#  IN the FILENAME, and NOT the creation date.
						#  doesn't matter unless $remove_old_files
						#  is 2 or 3.  
						#  If you run a backup once a day then set
						#  $remove_old_files = 2 and $date_check = 7
						#  to keep a weeks worth of backup files
						#  If you want to run a backup multible times a day
						#  then set $remove_old_files = 3 and $date_check = 7
						#  This will keep a weeks worth of files for
						#  EACH time the script is run IF a different
						#  ARG[1] value is used each time.  See comments under
						#  $remove_old_files (above)




#####################
# END VARS SECTION
#####################



# input the backup list into $backup_list
chomp($ARGV[0]);
$list_name = $ARGV[0];
chomp($list_name);
$list_name =~ s/[^a-zA-Z0-9-_\.\/]//g;  # no weird stuff in the filename
$backup_list = "$list_name";    # path to backup list file 

$date = `$date_path`;    # get the date into $date

# this is for ppl who fortget the end "/" on the backup dir
# and the scp remore dir
if ($backup_dir !~ /\/$/) {
	$backup_dir = $backup_dir . "/";
}
if ( $scp_remote_dir !~ /\/$/) {
	$scp_remote_dir = $scp_remote_dir  . "/";
}


#####################
# MAIN ROUTINE
#####################


# User enters command to see if the date command works
chomp($ARGV[1]);
$test_day = int($ARGV[1]);
if ($ARGV[0] eq "--testdate") {
	if ( $test_day > 0 && $test_day < 101 ) {
		if ( $test_day == 1) {
			print "\n\n==============================\n\n";
			print "\n\nDate for $test_day day ago is: ";
			$temp = `$date_path --date \'$test_day day ago\' $date_format`;
			print "$temp\n\n";
		} else {
			print "\n\n==============================\n\n";
			print "\n\nDate for $test_day days ago is: ";
			$temp = `$date_path --date \'$test_day days ago\' $date_format`;
			print "$temp\n\n";
		}
		print "If this looks right, then the date command is working properly.";
		print "\n\n==============================\n\n";
		exit;
	} else {
		print "\n\nPlease enter an integer [1-100]";
		print "\nFormat: /path/to/plbackitup.pl --testdate [1-100]";
		print "\n\n==============================\n\n";
		exit;
	}

}


# if $list_name is empty then exit.  No need to run.
if ($list_name =~ /^\s*$/ || $list_name =~ /^-h$/ || $list_name =~ /^--help$/) {
	print "\n\n==============================";
	print "\n\nNo backup_list specified.";
	print "\n\nFormat is:       \"/path/to/plbackitup.pl /path/to/backup_list [uniquename]\"";
	print "\n                  [uniquename] is optional.";
	print "\n\nTo test the date format enter \"/path/to/plbackitup.pl --testdate [1-100]";
	print "\n\nExiting.\n\n==============================\n\n";
	exit;
}



chomp($date);
$ftp_file_list = '';
$scp_file_list = '';

# ARGV only useful if using $remove_old_files = 3
if ($remove_old_files == 3) {
	$unique_name = $ARGV[1];
	chomp($unique_name);
	$unique_name =~ s/[^a-zA-Z0-9-_]//g; # no weird stuff in the filename
}


if (!-e $backup_dir) {
	print "\n\n==============================";
	print "\n\nThe DIR $backup_dir in the VAR \$backup_dir does not exist.";
	print "\n\nExiting.\n\n==============================\n\n";
	exit;
}

if (!-e $tar) {
	print "\n\nWrong path to \"tar\" or not installed.";
	print "\n\nExiting.\n\n==============================\n\n";
	exit;
}

if ($use_scp == 1 && !-e $scp_path) {
	print "\n\nWrong path to \"scp\" or not installed.";
	print "\n\nExiting.\n\n==============================\n\n";
	exit;
}

print "\n\n==============================\n\n##### Plbackitup running #####\nDate: $date\n\n";

# format the date stamp for the file
$filedate = `$date_path $date_format`;
chomp($filedate);
$filedate =~ s/\s//g;  # no spaces in there (like on the 1st of the month) 


#uncomment the below line if you want the date
#in the filename in lowercase
#($filedate = $filedate) =~ tr/A-Z/a-z/;


#######################
# Remove files routine
#######################

#the lines below remove old files in the backup
#directory with the extension $file_end
#depending on the value of $remove_old_files

# see if there are any files in the backup dir
# $backup_dir so we don't get that silly error "no such file or DIR"
$found_files = 0;
$check_for_files = `$dir -t $backup_dir`;
@CHECK_FOR_FILES = split /\s/, $check_for_files;
chomp(@CHECK_FOR_FILES);
foreach $temp(@CHECK_FOR_FILES) {
	if ($temp =~ "$file_end") {
		$found_files = 1;
	}
}

if ($remove_old_files != 0 && $found_files ==1 ) { 
	# the -t is "time-index", don't change this.
	$exist_files = `$dir -t $backup_dir*$file_end`;
}

if ($exist_files !~ /^\s*$/) {
	if ( $remove_old_files == 1 && $found_files == 1 ) {
		print "Deleting all files:  $backup_dir*$file_end\n";
		`$delete $backup_dir*$file_end  > /dev/null 2>&1`;
		$deleted_files = "\n=========== Deleted all files *$file_end ===========\n\n";
	} elsif ($remove_old_files == 2 ) {
		&keep_date;
	} elsif ($remove_old_files == 3 ) {
		&keep_using_unique;
	} 

} # end if $exist_files


#####################
# Main while routine
#####################

$output = <<OUTPUT;

******************************************************
$date
******************************************************
OUTPUT

#check the baclup_list file format first
$count_bad_lines = 0;
$count_good_lines = 0;
open (BACKUPLIST, $backup_list) or
  die "Could not open file \"$backup_list\"\nPlease check your path.\n\nExiting.\n\n==============================\n\n";
while ($line = <BACKUPLIST>) {
	chomp($line);
	if ( $line !~ /^\s*$/ && $line !~ "#" ) {
		if ( $line !~ /^([a-zA-Z0-9_\-\.]*)\:([a-zA-Z0-9_\.\-\/]*)\:/) { #no blanks or comments
			if ($count_bad_lines == 0 ) {$last_invalid_line = $line;}
			$count_bad_lines++;
		} else {
			$count_good_lines++;
		}
	}
} # end while
close(BACKUPLIST);
if ( $count_bad_lines > 0 ) {
	print "\nThe line below in the backup list \nfile \"$backup_list\" is being ignored\n";
	print "since it is in the wrong format.\n";
	print "Please fix it prior to backing up:\n\n";
	print "$last_invalid_line\n\n";
	print "Total invalid lines is $count_bad_lines\n\nExiting.\n\n==============================\n\n";
	exit;
} #end if
if ($count_good_lines == 0) {
	print "\nNo valid backup lines found.";
	print "\nPlease check the file \"$backup_list\"";
	print "\n(might be a directory)";
	print "\n\nExiting.\n\n==============================\n\n";
	exit;
}


$output .= $deleted_files;
open (BACKUPLIST, $backup_list) or
  die "Could not open file \"$backup_list\"\nPlease check your path.\n\nExiting.\n\n==============================\n\n";
while ($line = <BACKUPLIST>) {
	chomp($line);
	if ( $line !~ /^\s*$/ && $line !~ "#" ) {
		if ( $line =~ /^([a-zA-Z0-9_\-\.]*)\:([a-zA-Z0-9_\.\-\/]*)\:/) { #no blanks or comments
			@info = split /:/, $line;
			chomp (@info);
			$partialname = $info[0];
			$backup_path = $info[1];
			$exclusions = $info[2];
			if ($unique_name =~ /^\s*$/) {
				$file_name = $filedate . $fd . $partialname . $file_end;
			} else { 
				$file_name = $filedate . $fd . $unique_name . $fd . $partialname . $file_end;
			}
			$excludes = "";
			$exclude_list = "";	
			if ($exclusions !~ /^\s*$/) {
				@EXCLUDE = split /\s+/,$exclusions;
					foreach $line (@EXCLUDE) {
						chomp($line);
						$excludes .= " $tar_excl_cmd $line";
						$exclude_list .= "$line ";
					} # end foreach
			} # end if
			
			&tarthefile; # tar 'em up
		} else {
			# should never run because of other check above, but......
			print "\nThe line below in the backup list file $backup_list is being ignored, wrong format.";
			print "\nPlease fix it prior to backing up:";
			print "\n\n$line";
			print "\n\nExiting.\n\n==============================\n\n";
			exit;
		}


	} #end if-else
} #end while
close(BACKUPLIST);

#get the entire dir if $scp_all_files or $ftp_all_files = 1
if ($scp_all_files == 1 || $ftp_all_files == 1) {
	$temp_holder = '';
	$dir_list = `$dir $backup_dir*$file_end`;
	@DIR_LIST = split /\n/,$dir_list;
	foreach $filename(@DIR_LIST) {
		chomp($filename);
		$temp_holder .= "$filename\n";

	}

	if ($scp_all_files == 1) {
		$scp_file_list = $temp_holder;
	}
	if ($ftp_all_files == 1) {
		$ftp_file_list = $temp_holder;	
	}
} # end if

# ftp the files if enabled
if ($use_ftp == 1) {
	&ftp_the_files;
}

# scp the files if enabled
if ($use_scp == 1) {
	&scp_the_files;
}

$output .= <<OUTPUT;
******************************************************
******************************************************

OUTPUT


&write_log;

if ($email_log == 1 ) {
	if (-e "$backup_log") {
		&email_the_log;
	} else {
		print "\nNo log to email. Turn logging on.\n\n";
	}
}

print "\nDONE!\n==============================\n\n";
exit;

# end of main routine
####################


####################
# Subroutines Below
####################


####################
####################
#
# sub to print some output and tar the file

sub tarthefile {



$output .= <<OUTPUT;
Backup: $backup_path to file $backup_dir$file_name
OUTPUT

	if ($exclude_list =~ /^\s*$/) {
		$output .= "Excluding: none\n";
		$excludes_print = "Excluding: none";
	} else {
		$output .= "Excluding: $exclude_list\n";
		$excludes_print = "Excluding: $exclude_list";
	}


###backup the file now
if (-e "$backup_dir$file_name") {
	print "File $backup_dir$file_name exists --> OVERWRITING FILE <--\n";
}
if ($type_log == 2) {
	print "Backing up DIR: $backup_path TO FILE: $backup_dir$file_name $excludes_print\n";
	$output .= `$tar $tar_options $backup_dir$file_name $backup_path $excludes `;
} else {
	print "Backing up DIR: $backup_path TO FILE: $backup_dir$file_name $excludes_print\n";
	`$tar $tar_options $backup_dir$file_name $backup_path $excludes  > /dev/null 2>&1`;
}

# save the list of files to ftp
$ftp_file_list .= "$backup_dir$file_name\n";

# save the list of files to scp
$scp_file_list .= "$backup_dir$file_name\n";

} # end sub tarthefile


####################
#
# sub to write the log file

sub write_log {



	## print the results to a file
	if ($type_log != 0) {
		`$touch $backup_log`;
		
		if ($overwrite_log == 1) {
			print "Writing log file $backup_log Mode: OVERWRITE\n";
			open(FILE,">".$backup_log) || die "can't open $backup_log\n";
		} else {
			print "Writing log file $backup_log Mode: APPEND\n";
			open(FILE,">>".$backup_log) || die "can't open $backup_log\n";
		}
		print FILE $output;
		close(FILE);
	}

} # end sub write_log


####################
#
# sub to email the log

sub email_the_log {

	if (!-e $email_path) {
		print "Email program $email_path not found. Please check PATH VAR \$email_path.";
		
	} else {
		 print "Emailing the log file: $backup_log To: $email_to\n";
		`$email_path -s "Backup log for $date"  $email_to < "$backup_log"`;
	}

} # end sub email_the_log


####################
#
# sub to keep files in the past
# $date_check days ago
#

sub keep_date {

$deleted_files = "\n================= Deleted Files =================\n";

$counter=0;
while ($counter <= $date_check+1) {
$days = $date_check - $counter;
if ($days == 1) {$temp_arg = "--date \'$days day ago\' $date_format";}
 else {$temp_arg = "--date \'$days days ago\' $date_format";}

$DATE_LIST[$counter] = `$date_path $temp_arg`;
($DATE_LIST[$counter] = $DATE_LIST[$counter]) =~ s/\s//g; # no spaces in there

$counter++;
}

$dir_list = `$dir -t1 $backup_dir*$files_end`;
#print "\nDir List:\n$dir_list\n\n"; # for debugging
@DIR_LIST = split /\n/,$dir_list;

foreach $dirline(@DIR_LIST) {
        $check=0;
        chomp($dirline);
        foreach $dateline(@DATE_LIST) {
                chomp($dateline);
                if ($dirline =~ /$dateline/i && $dirline =~ /$file_end/i) {
                        $check = 1;
                }
        }
                if ($check == 0 && $dirline =~ $file_end) {
                        # the extra / below is for ppl who forget
                        # the trailing / in $backup_dir
                        # .....shouldn't hurt anything
                        $deleted_files .= "$dirline\n";
			print "Deleting file: $dirline\n";
                        `$delete $dirline`;
                }

}

$deleted_files .= "================= Deleted Files =================\n\n";

} # end sub keep_date


####################
#
# sub to keep the number of each backup file
# regardless of date stamp in the filename
#
sub keep_using_unique {
 

$deleted_files = "\n================= Deleted Files =================\n";
 
$counter=0;
while ($counter <= $date_check+1) {
$days = $date_check - $counter;
if ($days == 1) {$temp_arg = "--date \'$days day ago\' $date_format";}
 else {$temp_arg = "--date \'$days days ago\' $date_format";}
 
$DATE_LIST[$counter] = `$date_path $temp_arg`;
($DATE_LIST[$counter] = $DATE_LIST[$counter]) =~ s/\s//g; # no spaces in there
 
$counter++;
}
 
$dir_list = `$dir -t1 $backup_dir*$files_end`;
#print "\nDir List:\n$dir_list\n\n"; # for debugging
@DIR_LIST = split /\n/,$dir_list;
 
foreach $dirline(@DIR_LIST) {
        $check=0;
        chomp($dirline);
        foreach $dateline(@DATE_LIST) {
                chomp($dateline);
                if ($dirline =~ /$dateline/i && $dirline =~ /$file_end/i && $dirline =~ /$unique_name/i ) {
                        $check = 1;
                }
        }
                if ($check == 0 && $dirline =~ $file_end && $dirline =~ $unique_name ) {
                        # the extra / below is for ppl who forget
                        # the trailing / in $backup_dir
                        # .....shouldn't hurt anything
                        $deleted_files .= "$dirline\n";
                        print "Deleting file: $dirline\n";
                        `$delete $dirline`;
                }
 
} # end sub keep_files_using_unique
 
$deleted_files .= "================= Deleted Files =================\n\n";  
 
 
} # end sub keep_using_unique


######################
#
# sub to FTP the files 
# somewhere else
#

sub ftp_the_files {

print "Attempting to send files to FTP server.\n";

$cant_connect =0;
$cant_login = 0;
$remote_chdir = 0;
$ftp_log = "====================== FTP ======================\n";
print "\n\n====================== FTP ======================";
if ($ftp_debug == 0) {
	print "\n(for verbose FTP output set \$ftp_debug = 1 or 2)\n";
} else {
	print "\n(verbose output)\n";
}

#open the connection
print "\nAttempting to FTP backup files to host \"$ftp_hostname\"\n";

$ftp = Net::FTP->new($ftp_hostname,
		Timeout => $ftp_timeout,
		Debug => $ftp_debug)   # Debug 1 or 2 will output info to the screen
		or $cant_connect = 1;
	if ($cant_connect == 0) {
		if ($ftp_debug == 0) {print "\nConnected to FTP server \"$ftp_hostname\"";}
		 $ftp_log .= "Connected to FTP server \"$ftp_hostname\"\n";
	}
	if ($cant_connect != 1) {
		$ftp->login($ftp_username, $ftp_password)
			or $cant_login = 1;
		if ($cant_login == 0) {
			if ($ftp_debug == 0) {print "\nLogged into FTP server \"$ftp_hostname\" with user name \"$ftp_username\"";}
			$ftp_log .= "Logged into FTP server \"$ftp_hostname\" with user name \"$ftp_username\"\n";
		}
		if ($cant_login != 1) {
			$ftp->binary(); # set binary mode
			$ftp->cwd($ftp_remote_dir)
				or $remote_chdir = 1;
			if ($remote_chdir == 0) {
				if ($ftp_debug == 0) {print "\nChanged to remote DIR \"$ftp_remote_dir\"";}
				$ftp_log .= "Changed to remote DIR \"$ftp_remote_dir\"\n";
			}
			if ($remote_chdir != 1) {

				#if ($remove_old_ftp_files != 0) {  #will be added at a later date
				#	&remove_ftp_files;
				#}

				@FTP_FILE_LIST = split /\n/,$ftp_file_list;
				foreach $filename(@FTP_FILE_LIST) {
					$no_put_file = 0;
					chomp($filename);
					#$full_local_path = "$backup_dir/$filename";
					$full_local_path = "$filename";
					$ftp->put($full_local_path) 
						or $no_put_file = 1; 
					 if ($no_put_file != 1) {
						$ftp_log .= "FTP: Put file $filename\n";
						if ($ftp_debug == 0) {print "\nPut file $filename";} 
					 } else {
						$ftp_log .= "Couldn't upload $filename -> Permission Denied.\n";  
						if ($ftp_debug == 0) {print "\nCouldn't upload $filename -> Permission Denied.";}
					} 
				} # end foreach $filename
			} else {
				$ftp_log .= "Couldn't cwd to $ftp_remote_dir\n";
				if ($ftp_debug == 0) {print "\nCouldn't cwd to DIR \"$ftp_remote_dir.\"";}
			}
		} else {
			$ftp_log .= "Couldn't log into FTP server $ftp_hostname with user name \"$ftp_username\"\n";
			if ($ftp_debug == 0) {print "\nCouldn't log into FTP server \"$ftp_hostname\" with user name \"$ftp_username\"";}
		}	
	} else {
		$ftp_log .= "Couldn't connect to FTP server $ftp_hostname\n";
		if ($ftp_debug == 0) {print "\nCouldn't connect to FTP server \"$ftp_hostname\"";}
	}

	$ftp->quit; # close ftp session

#if ($remove_old_ftp_files != 0) {
#	$ftp_log = $ftp_log . $ftp_delete_files_log;
#}

$ftp_log .= "Done!\n====================== FTP ====================== \n\n";
print "\nDone!\n====================== FTP ======================\n\n";
$output = $output . "\n$ftp_log";

#print "\n\n$FTP_ERROR\n\n";

} # end sub ftp_the_files

######################
#
# SCP the files somewhere else
#
#
# This may not work.  
# There are lots of permissions issues involved

sub scp_the_files {

$scp_log = "====================== SCP ====================== \n";
print "\n\n====================== SCP  ======================";
print "\nAttempting to secure copy the backup files to \"$scp_hostname\"\n";
print "\nFile list:\n";
	@SCP_FILE_LIST = split /\n/,$scp_file_list;
	if ($scp_all_files == 0 ) {
		foreach $filename(@SCP_FILE_LIST) {
			chomp($filename);
			print "\nscp $filename -> $scp_hostname:$scp_remote_dir";
			$scp_log .= "scp $filename -> $scp_hostname:$scp_remote_dir\n";
			`$scp_path $filename $scp_username\@$scp_hostname:$scp_remote_dir`;
		}
	} else {
		`$scp_path $backup_dir*file_end  $scp_username\@$scp_hostname:$scp_remote_dir`;
		print "\nscp all files $backup_dir*file_end -> $scp_hostname:$scp_remote_dir";
		$scp_log .= "scp all files $backup_dir*file_end -> $scp_hostname:$scp_remote_dir\n";
	}


	$scp_log .= "\n====================== SCP ====================== \n\n";
	$output = $output . "\n$scp_log";
print "\n\nDone!\n";
print "====================== SCP  ======================\n\n";


} # end sub scp_the_files


#### end of script ####





